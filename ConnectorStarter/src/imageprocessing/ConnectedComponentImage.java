package imageprocessing;

import java.awt.Color;

import edu.princeton.cs.introcs.Picture;

public class ConnectedComponentImage {
	int BoundingBox;
	Picture picture;
    Picture fileLocation;
    int height;
    int width;
    int threshhold = 125;
	public ConnectedComponentImage(Picture fileLocation) 
	{
		this.fileLocation = fileLocation;
	}

	private WeightedQuickUnionUF checkUnion(Picture pic)
	{
		width = pic.width();
		height = pic.height();

		WeightedQuickUnionUF wqu = new WeightedQuickUnionUF(width*height);
		for (int y = 0; y < height; y++) 
		{
			for (int x = 0; x < width; x++) 
			{
				if( y+1 < height
						&& pic.get(x, y).equals(pic.get(x, y+1)) )
				{
					wqu.union(index(x, y), index(x,y+1));
				}
				if( x+1 < width
						&& pic.get(x, y).equals(pic.get(x+1, y)) ) 
				{
					wqu.union(index(x, y), index(x+1,y));
				}
			} 
		}
		return wqu;
	}
	public int countComponents() 
	{
	WeightedQuickUnionUF wqu = checkUnion(binaryComponentImage());
	return wqu.count()-1; 
		
	}
	public int index(int x, int y)
	{
		return (y*(width)) + x;
	}
	
	public Picture identifyComonentImage() {

		Picture pic =  picture;

		int maxX = 0;
		int minX = pic.width();
		int maxY = 0;
		int minY = pic.height();

		for (int x = 0; x < pic.width(); x++) {
			for (int y = 0; y < pic.height(); y++) {
				if (!pic.get(x, y).equals(Color.WHITE) ) {

					if (x < minX)
						minX = x;
					if (x > maxX)
						maxX = x;
					if (y < minY)
						minY = y;
					if (y > maxY)
						maxY = y;

				}
			}

		}

		if (minX > maxX || minY > maxY) {
			System.out.println("It's All White Pixels!!!");
		} else {
			for (int x = minX; x <= maxX; x++) {
				pic.set(x, minY, Color.RED);
				pic.set(x, maxY, Color.RED);
			}

			for (int y = minY; y <= maxY; y++) {
				pic.set(minX, y, Color.RED);
				pic.set(maxX, y, Color.RED);
			}
		}
		return pic;
	}

	
	public Picture colourComponentImage() {
		return picture;
	}
	
		
	public void setThreshhold(int threshhold) 
	{
		this.threshhold = threshhold;
	}
	
	public int getThreshhold() 
	{
		return threshhold;
	}
	
	
	
	public Picture getPicture() {
		
		return picture;
	}

	
	public Picture binaryComponentImage() {
		Picture pic =  picture;
		
		return pic;
		

	}

}


	
